package ic.kotlinc


import ic.base.throwables.CompilationException
import ic.base.throwables.NotNeededException
import ic.storage.fs.Directory
import ic.storage.fs.local.impl.funs.setFilePermissions
import ic.struct.collection.Collection
import ic.struct.sequence.ext.foreach.forEach
import ic.system.funs.executeBashScript
import ic.text.SplitText


@Throws(CompilationException::class, NotNeededException::class)
fun compileKotlin (

	inputDir	: Directory,
	outputDir	: Directory,

	compiledDependencyDirs	: Collection<Directory>,
	jarDependenciesDirs 	: Collection<Directory>,

	jdkHome : String? = null,
	javaVersion : String,

	handleWarning : (String) -> Unit

) {

	val script = generateKotlinCompileScript(
		inputDir = inputDir,
		outputDir = outputDir,
		compiledDependencyDirs = compiledDependencyDirs,
		jarDependenciesDirs = jarDependenciesDirs,
		jdkHome = jdkHome,
		jvmVersion = javaVersion
	)

	val response = executeBashScript(script)

	SplitText(response, '\n').forEach { line ->
		when {

			line.isEmpty -> return@forEach

			line.startsWith("WARNING: ") -> return@forEach

			line.contains(": warning: ") -> handleWarning(line.toString())
			line.contains(": info: ") -> handleWarning(line.toString())
			line.startsWith("import ") -> handleWarning(line.toString())
			line.startsWith("class ") -> handleWarning(line.toString())
			line.startsWith("abstract class ") -> handleWarning(line.toString())
			line.startsWith("interface ") -> handleWarning(line.toString())
			line.startsWith("object ") -> handleWarning(line.toString())
			line.startsWith("fun ") -> handleWarning(line.toString())
			line.startsWith("operator ") -> handleWarning(line.toString())
			line.startsWith("infix fun ") -> handleWarning(line.toString())
			line.startsWith("val ") -> handleWarning(line.toString())
			line.startsWith("inline ") -> handleWarning(line.toString())
			line.startsWith("internal ") -> handleWarning(line.toString())
			line.startsWith("private ") -> handleWarning(line.toString())
			line.startsWith("typealias ") -> handleWarning(line.toString())
			line.startsWith(")") -> handleWarning(line.toString())
			line.startsWith("^") -> handleWarning(line.toString())
			line.startsWith("@") -> handleWarning(line.toString())
			line.startsWith(" ") || line.startsWith("\t") -> handleWarning(line.toString())

			line.equals("error: no source files") -> throw NotNeededException

			else -> throw CompilationException("$response\nLINE: $line")

		}
	}

	setFilePermissions(outputDir.absolutePath, isRecursive = true)

}