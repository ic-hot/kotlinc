package ic.kotlinc


import ic.base.strings.ext.asText
import ic.cmd.bash.bashPureSingleQuote
import ic.storage.fs.Directory
import ic.struct.collection.Collection
import ic.struct.collection.ext.copy.copyConvert
import ic.struct.collection.ext.copy.copyConvertToList
import ic.struct.list.ext.concat
import ic.struct.list.ext.copy.copyJoin
import ic.struct.list.ext.plus
import ic.text.Text


fun generateKotlinCompileScript(

	inputDir: Directory,
	outputDir: Directory,

	compiledDependencyDirs : Collection<Directory>,
	jarDependenciesDirs : Collection<Directory>,

	jdkHome : String? = null,
	jvmVersion : String

) : Text {

	val script = (
		"shopt -s globstar;" +
		"export JAVA_OPTS='-Xmx8g';" +
		"kotlinc " + (
			(
				if (jdkHome == null) {
					""
				} else {
					"-jdk-home $jdkHome "
				}
			) +
			"-jvm-target $jvmVersion " +
			"-Xjvm-default=all " +
			"-classpath " + (
				(
					compiledDependencyDirs.copyConvertToList {
						bashPureSingleQuote(it.absolutePath)
					} +
					jarDependenciesDirs.copyConvertToList { jarDependency ->
						jarDependency.getItems().copyConvert {
							bashPureSingleQuote(it.absolutePath)
						}
					}.copyJoin()
				).concat(':')
			) + " " +
			bashPureSingleQuote(inputDir.absolutePath) + "/ " +
			"-d " + bashPureSingleQuote(outputDir.absolutePath)
		)
	)

	return script.asText

}